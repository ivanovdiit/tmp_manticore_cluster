В proxysql/proxysql.cnf конфиг, за основу был взят тот который устанавливается по умолчанию

# Запуск:

в папке сделать docker-compose up

# Чтоб подключиться к proxysql в режиме проксирования

docker-compose exec proxysql mysql -uroot -h0 -P6033

вот в нем если я использую manticore у меня прекрасно работает `select` но не работает `show tables` и `insert`

# Чтоб подключиться к админ части ProxySQL

docker-compose exec proxysql mysql -uroot -h0 -P6032


# Чтоб использовать MySQL
 
В proxy.cnf необходимо раскомментировать 

```
      { address="mysql" , port=3306 , hostgroup=0 }
```
и закомментировать
```
{ address="manticore2" , port=9306 , hostgroup=0 }
```

# Чтоб использовать 2 инстанса Manticore 

В proxy.cnf необходимо раскомментировать
```
{ address="manticore1" , port=9306 , hostgroup=0 },
```

# Попасть внутрь контейнера с proxysql

docker-compose exec proxysql bash
